/* Continuous Reinforcement (Fixed Ratio 1)
Thiago Gouvea, Dec 2015 */

// Tweakable parameters

int ITI = 2500; // (ms)
int timeValvL = 100; // Calibrate valves for .85 ul
int timeValvR = 92; // Calibrate valves for .85 ul

// PinOut
const int syncLed = 9;
const int syncEphys = 8;
const int syncStim = 7;
const int laser = 6; // output pin supporting PWM
const int spkr = 10;
const int pkC1 = 3;
const int ledC1 = 23;
const int pkC2 = 19;
const int ledC2 = 26;
const int pkL = 21;
const int ledL = 24;
const int valvL = 30;
const int pkR = 2;
const int ledR = 22;
const int valvR = 32;
//int syncLed = 2;//22;
//int spkr = 3;
//int pkC = 8;
//int ledC = 9;
//int pkL = 5;
//int ledL = 6;
//int valvL = 7;
//int pkR = 11;
//int ledR = 12;
//int valvR = 4;
//int laser = 44; //2;
////int aoPIN = 44;

// Internal variables
int state;
int trialNum = 0;
int trialCount = 0;
boolean rwdL = false;
boolean rwdR = false;
boolean valuePkC;
boolean valuePkC1;
boolean valuePkC2;
boolean valuePkL;
boolean valuePkR;
boolean valuePkC1now;
boolean valuePkC2now;
boolean valuePkLnow;
boolean valuePkRnow;

unsigned long swTrialOn;
unsigned long swRwdOn;

void setup(){
  pinMode(0,INPUT);
  randomSeed(analogRead(0));
  pinMode(syncLed,OUTPUT);
  pinMode(spkr,OUTPUT);
  pinMode(pkC1,INPUT);
  pinMode(ledC1,OUTPUT);
  pinMode(pkC2,INPUT);
  pinMode(ledC2,OUTPUT);
  pinMode(pkL,INPUT);
  pinMode(ledL,OUTPUT);
  pinMode(valvL,OUTPUT);
  pinMode(pkR,INPUT);
  pinMode(ledR,OUTPUT);
  pinMode(valvR,OUTPUT);
  pinMode(laser,OUTPUT);

  digitalWrite(ledL,HIGH);
  digitalWrite(ledC1,HIGH);
  digitalWrite(ledC2,HIGH);
  digitalWrite(ledR,HIGH);
  digitalWrite(syncLed,HIGH);
  digitalWrite(valvL,LOW);
  digitalWrite(valvR,LOW);
  analogWrite(laser,0);

  Serial.begin(115200);
  while(Serial.read() != 115){
  }

  state = 22;
  Serial.println(String(state) + '\t' + String(millis()));
}

void loop() {
  updatePokes();
  stateMachine();  
}

// Read pokes
void updatePokes() {
  valuePkC1now = digitalRead(pkC1);
  valuePkC2now = digitalRead(pkC2);
  valuePkLnow = digitalRead(pkL);
  valuePkRnow = digitalRead(pkR);
  if (valuePkC1 != valuePkC1now) {
    if (valuePkC1now) {
      Serial.println(String(0) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(8) + '\t' + String(millis()));
    }
    valuePkC1 = valuePkC1now;
  }
  if (valuePkC2 != valuePkC2now) {
    if (valuePkC2now) {
      Serial.println(String(140) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(141) + '\t' + String(millis()));
    }
    valuePkC2 = valuePkC2now;
  }
  if (valuePkL != valuePkLnow) {
    if (valuePkLnow) {
      Serial.println(String(1) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(9) + '\t' + String(millis()));
    }
    valuePkL = valuePkLnow;
  }
  if (valuePkR != valuePkRnow) {
    if (valuePkRnow) {
      Serial.println(String(2) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(10) + '\t' + String(millis()));
    }
    valuePkR = valuePkRnow;
  }
}

// Run state machine
void stateMachine(){
  switch (state) {
  case 22: // state_0 state
    // This is a hack. It is not clear why this timer needs to be reset each time.
    TCCR5B = (TCCR5B & 0xF8) | 0x01; // set Timer to 32kHz //http://sobisource.com/?p=195

    digitalWrite(syncLed,LOW);
    Serial.println(String(61) + '\t' + String(millis()));
    delay(100);
    digitalWrite(syncLed,HIGH);
    Serial.println(String(62) + '\t' + String(millis()));

    trialNum++;
    trialCount++;

    digitalWrite(ledL,LOW);
    Serial.println(String(4) + '\t' + String(millis()));
    digitalWrite(ledR,LOW);
    Serial.println(String(5) + '\t' + String(millis()));
    state = 25;
    Serial.println(String(state) + '\t' + String(millis()));
    tone(spkr,7000,150); // comment out
    break;

  case 25: // wait_Sin state
    if(valuePkL){
      swTrialOn = millis();
      state = 26;
      Serial.println(String(state) + '\t' + String(millis()));
      Serial.println(String(12) + '\t' + String(millis()));
      digitalWrite(ledL,HIGH);
      Serial.println(String(13) + '\t' + String(millis()));
      digitalWrite(ledR,HIGH);
    }
    if(valuePkR){
      swTrialOn = millis();
      state = 28;
      Serial.println(String(state) + '\t' + String(millis()));
      Serial.println(String(12) + '\t' + String(millis()));
      digitalWrite(ledL,HIGH);
      Serial.println(String(13) + '\t' + String(millis()));
      digitalWrite(ledR,HIGH);
    }
    break;

  case 26: // correct_Lin state
    state = 30;
    Serial.println(String(state) + '\t' + String(millis()));
    break;

  case 28: // correct_Rin state
    state = 31;
    Serial.println(String(state) + '\t' + String(millis()));
    break;

  case 30: // water_L state
    rwdL = false;
    digitalWrite(valvL,HIGH);
    Serial.println(String(6) + '\t' + String(millis()));
    delay(timeValvL);
    digitalWrite(valvL,LOW);
    Serial.println(String(14) + '\t' + String(millis()));
    swRwdOn = millis();
    state = 32;
    Serial.println(String(state) + '\t' + String(millis()));
    break;

  case 31: // water_R state
    rwdR = false;
    digitalWrite(valvR,HIGH);
    Serial.println(String(7) + '\t' + String(millis()));
    delay(timeValvR);
    digitalWrite(valvR,LOW);
    Serial.println(String(15) + '\t' + String(millis()));
    swRwdOn = millis();
    state = 32;
    Serial.println(String(state) + '\t' + String(millis()));
    break;

  case 32: // ITI state
    if((millis()-swTrialOn)>ITI){
      state = 22;
      Serial.println(String(state) + '\t' + String(millis()));
    }
    break;
  }  
}
